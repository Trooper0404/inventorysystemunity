﻿// Matt Radke
//mmr174
// cmpt 306, individual project: 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this is our backPack, it controls and maintaines the playerInventory. 
public class backPackManager : MonoBehaviour {


	private Dictionary<string, List<item>> backPack; 
	private int capacity; // capacity of the inventory, when we first start off, we will use the sizeOfBackPack, then if players find more items to add onto the BackPack the capacity will increase.
	public int sizeOfBackPack; // starting size of the BackPack.



	public bool addToBackPack(string Name, item newItem){

		if (capacity == this.returnBPListcount ()) {
			return false; 

		} else {
		
			if (backPack.Count == 0) {
			
				List<item> newListOfItems = new List<item> (); 
			
				newListOfItems.Add (newItem); 
			
				backPack.Add(Name, newListOfItems);

				return true; 
			
			
			} else if (backPack.Count != 0) {
			
			
				// bug here make sure to fix
			
				if (backPack.ContainsKey (Name) && capacity != this.returnBPListcount()) {
				
					backPack [Name].Add (newItem); 

					return true; 

				
				} else if (!backPack.ContainsKey(Name) && capacity != this.returnBPListcount()) {
				
				
					List<item> newListOfItems = new List<item> (); 
				
					newListOfItems.Add (newItem);
				
					backPack.Add(Name, newListOfItems); 
				
					//BP.GetComponent<backPackManager>().printOutEntireBackPack(); 
					return true; 
				
				
				}

					
					
			}
			return false;


			
		}
		

	}



	public Dictionary<string, List<item> > returnDictionary(){


		return backPack; 

	}


	public void printOutEntireBackPack(){


		foreach (KeyValuePair<string, List<item>> counter in backPack) {
			int k = 0;

			while(k < counter.Value.Count){

				print (counter.Value[k].returnName());

				k++; 

			}

		}



	}

	public void removeItems(string nameOfItem){

		backPack [nameOfItem].RemoveAt (0);  // always the first item in the list.(we will always know there has to be atleast 1 item by the time this function is called.)
	


	}


	public int returnBackPacksCount(){


		return backPack.Count; 
	}
	 

	public int returnBPListcount(){
		// return number of all items in the backPack.
		int totalItem = 0; 

		foreach (KeyValuePair<string, List<item>> counter in backPack) {

			totalItem += counter.Value.Count;

		}


		return totalItem; 



	}

	public string[] returnBPKeys(){


		string [] keys = new string[backPack.Count]; 
		int k = 0; 
		foreach (KeyValuePair<string, List<item>> counter in backPack) {
		
			keys[k] = counter.Key; 

			k++; 
		}

		return keys; 

	}

	public int returnBackPackCapcity(){

		return capacity; 
	}

	// Use this for initialization
	void Start () {
		 
		// creates the backPack.
		backPack = new Dictionary< string, List<item> >();  
		capacity = sizeOfBackPack; 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
