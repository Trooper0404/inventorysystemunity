﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// this classes purpose is to define what exactly is a Item, atleast in a game sense.
public class item {

	private string nameOfItem;
	private GameObject physicalItem; 
	public bool isItemInMenu = false; 

	// later, a list of craftable items? Or a array that can be combined with the items.



	/**
	 * item(string,GameObject):
	 * param:	name: string, the name that we want to give this item.
	 * 			item: GameObject, the gameobject that is assiocated with this item.
	 * 
	 * the constructor of items, this method will build the actual item.
	 * 
	 * return: N/A
	 *
	 * 
	 */ 
	public item(string name, GameObject item){

		nameOfItem = name;
		physicalItem = item; 
 

	}

	/**
	 * returnName():
	 * 
	 * returns the name of this item.
	 * 
	 * return: A string containing the name of this item.
	 */ 
	public string returnName(){

		return nameOfItem; 

	}


	/**
	 *GameObject returnPhysicalItem(): 
	 * 
	 * returns the gameObject of the item.
	 * 
	 * return: a GameObject of the item stored in the backPack.
	 * 
	 * 
	 */  

	public GameObject returnPhysicalItem (){


		return physicalItem; 
	}

	/**
	 * changeName(string):
	 * param:	newName: string, a new name that we want to give this item.
	 * mutator method, changes the name of the current item.
	 * return: N/A
	 * 
	 */ 
	public void ChangeName(string newName){

		this.nameOfItem = newName; 


	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
