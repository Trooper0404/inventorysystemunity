﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.IO; // used to count through all the prefabs in a directory.


public class bluePrints : MonoBehaviour {

	private Dictionary<string, GameObject> bluePrintLibrary; 
	public TextAsset keys; 
	public Object[] prefabs; 

	public void buildBluePrint(){
		prefabs = Resources.LoadAll ("bluePrintPrefab", typeof(GameObject));
		string transfer;
		string newLine = "\n";

		transfer = keys.text; 
		//string [] keysInFile = Regex.Split(transfer, newLine, RegexOptions.None); 

		//string [] keysInFile = transfer.Split('\n');
			
		string [] keysInFile = new string[2];

		keysInFile [0] = "clothrubbingalcohol";
		keysInFile [1] = "knifeshirt";
	
		for(int i = 0; i < keysInFile.Length; i++ ){
			print (keysInFile[i]);

			GameObject newItem = (GameObject) prefabs[i];



			bluePrintLibrary.Add(keysInFile[i], newItem);




		}

		print ("test:" + bluePrintLibrary ["clothrubbingalcohol"].name);

	}



	public Dictionary<string, GameObject> returnBlueprintsLib(){


		return bluePrintLibrary; 
	}

	public int returnBluePrintCount(){


		return bluePrintLibrary.Count; 
	}

	// Use this for initialization
	void Start () {

		bluePrintLibrary = new Dictionary<string, GameObject> (); 
		buildBluePrint (); 


		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
