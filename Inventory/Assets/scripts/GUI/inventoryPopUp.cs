﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this class handles building and enabling the PlayerInventory!

public class inventoryPopUp : MonoBehaviour {

	public GameObject playerInvententory; 
	// Use this for initialization
	void Start () {

		playerInvententory.SetActive (false);  // when we start the game, we want to make sure that the panel is off!

	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown (KeyCode.Tab)) {

			if(playerInvententory.activeSelf == false){

				playerInvententory.SetActive (true); 
			}else{

				playerInvententory.SetActive(false);

			}
		}
		
	}
}
