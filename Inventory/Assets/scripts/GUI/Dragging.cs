﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI; 
using System; 


// script which will be put on each button.
public class Dragging : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public GameObject bp;
	public GameObject questionPanel;
 

	[SerializeField] private GameObject itemBeingDragged;
	private Vector2 startPos;
	private GameObject backPackManager; 

	public void OnCollisionEnter2D(Collision2D coll){

		if (coll.gameObject.name.Equals ("emptySpace")) {


			Sprite imgholder = coll.gameObject.GetComponent<Button> ().GetComponent<Image> ().sprite; 
			Color colorHolder = coll.gameObject.GetComponent<Button> ().GetComponent<Image> ().color;
			string nameHolder = coll.gameObject.name; 


			if (itemBeingDragged != null) {

				itemBeingDragged.GetComponent<BoxCollider2D> ().enabled = false;

				coll.gameObject.GetComponent<Button> ().GetComponent<Image> ().sprite = itemBeingDragged.GetComponent<Button> ().GetComponent<Image> ().sprite; 
				coll.gameObject.GetComponent<Button> ().GetComponent<Image> ().color = itemBeingDragged.GetComponent<Button> ().GetComponent<Image> ().color;
				coll.gameObject.name = itemBeingDragged.name; 


				itemBeingDragged.GetComponent<Button> ().GetComponent<Image> ().sprite = imgholder;
				itemBeingDragged.GetComponent<Button> ().GetComponent<Image> ().color = colorHolder;
				itemBeingDragged.name = nameHolder;



			}

		} else if (!coll.gameObject.name.Equals ("emptySpace")) {

			if (itemBeingDragged != null && itemBeingDragged.GetComponent<BoxCollider2D> ().enabled != false) {

				questionPanel = this.GetComponentInParent<interactiveBackPack> ().returnQPanel (); 

				GameObject itemDragging = itemBeingDragged; 
				GameObject collidingObject = coll.gameObject; 



				questionPanel.GetComponentsInChildren<Button> () [0].onClick.AddListener ( delegate {


					combinBtnActList(itemDragging, collidingObject); 
				});
				
				questionPanel.GetComponentsInChildren<Button> () [1].onClick.AddListener (cancelBtnActList);


				questionPanel.SetActive (true); 



			}
		}
		


	}


	public void combinBtnActList(GameObject itemDragging, GameObject collidingObject){
		
		Dictionary<string, GameObject> bluePrints = bp.GetComponent<bluePrints>().returnBlueprintsLib();



		// need to add a translation function, which gives proper key depending what collision happened. 

		string bluePrintKey =  itemDragging.name + collidingObject.name;

		 
		string noSpace = bluePrintKey.Replace (" ", "" );
		//print ("noSpace before failure!"+noSpace);
		bool wasKeyFound = false; 

		int k = 0;
		foreach (KeyValuePair<string, GameObject> counter in bluePrints) {
			//print ("Searching ...");

			if(counter.Key.Equals(noSpace)){
				//print ("FOUND!"); 
				wasKeyFound = true;
				break; 

			}

			if(k == bluePrints.Count-1){
				//print ("Key not found!");
				bluePrintKey = collidingObject.name + itemDragging.name;
				noSpace = bluePrintKey.Replace(" ", ""); 
				//print ("this is what is inside: AFTER failure:" + noSpace);
			}

			k++;
		}


		if (wasKeyFound == false) {
			//print ("looks like key was not found, moving onto second type of key!");
			foreach (KeyValuePair<string, GameObject> counter in bluePrints) {
				
				if(counter.Key.Equals(noSpace)){
				//	print ("FOUND!"); 
					wasKeyFound = true;
					break; 
					
				}
			}


		}


		


		GameObject bluePrintGameObject = bluePrints [noSpace];

		item craftedItem = new item (bluePrintGameObject.name, bluePrintGameObject);


	


		backPackManager.GetComponent<backPackManager> ().removeItems (itemDragging.name); 
		backPackManager.GetComponent<backPackManager> ().removeItems (collidingObject.name); 

		backPackManager.GetComponent<backPackManager> ().addToBackPack (craftedItem.returnName (), craftedItem);

		this.GetComponentInParent<interactiveBackPack> ().removePic (collidingObject);
		this.GetComponentInParent<interactiveBackPack> ().removePic (itemDragging);
		
		questionPanel.GetComponentsInChildren<Button> () [0].onClick.RemoveAllListeners (); // data does not change unless you initlaize it EVERY TIME (Very nice 3 hour propblem.)

		questionPanel.SetActive (false);
		
		
	}







	public void cancelBtnActList(){

		questionPanel.SetActive (false);
		
		
	}




	public void OnDrag(PointerEventData eventData){

		if (!this.gameObject.name.Equals ("emptySpace")) {
			transform.position = Input.mousePosition; 
		} 

	}





	public void OnBeginDrag (PointerEventData eventData)
	{
		
		

		startPos = this.gameObject.transform.position; 
		itemBeingDragged = this.gameObject;


		bp = GameObject.FindGameObjectWithTag("bluePrint");
		backPackManager = GameObject.FindGameObjectWithTag("BPManager");
	
	}


	public void OnEndDrag (PointerEventData eventData)
	{

		 
		
		itemBeingDragged.transform.position = startPos;
		itemBeingDragged.GetComponent<BoxCollider2D>().enabled = true;
		itemBeingDragged = null;


	}





}
