﻿// Matt Radke
// mmr174
// CMPT306

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// creating the interactive Inventory:
public class interactiveBackPack : MonoBehaviour { 
	public GameObject prefab;	// the prefab we want to use when populating the scroll view.
	public GameObject qpPanel;


	private int numberOfObjects; // the number of objects we want inside the scroll view. 
	private GameObject backPackManager; // the max number of items that are allowed to be inside the backPack.
	private List<GameObject> listOfButtons = new List<GameObject>();
	private int newSize;


	/**
	 * void buildScroll():
	 * 
	 * builds the inventory scroll view, it take our button prefab and add all the buttons onto the scroll view.
	 * 
	 * return: Nothing.
	 * 
	 */ 
	public void buildScroll(){

		numberOfObjects = backPackManager.GetComponent<backPackManager> ().sizeOfBackPack;

		int counter = 0;
		
		while (counter < numberOfObjects) { // how many inventory slots are currently ready for use?
			GameObject newSpace;  // the button that is wanted to be  created?
			
			newSpace = Instantiate(prefab, this.transform); 

			newSpace.gameObject.GetComponentInChildren<Text>().text = " ";  // remove the text from the button.
			newSpace.gameObject.GetComponent<RectTransform>().SetParent(this.gameObject.GetComponent<RectTransform>(), false);
			newSpace.gameObject.name = "emptySpace";

			#pragma warning disable
			Dragging addDrag = newSpace.AddComponent<Dragging>();  
			#pragma warning enable

			// we want to create a boxcollider which we can collide with other children within the Panel.
			BoxCollider2D box = newSpace.AddComponent<BoxCollider2D>();
			float ratioX = newSpace.GetComponentInParent<GridLayoutGroup>().cellSize.x - 5;
			float ratioY = newSpace.GetComponentInParent<GridLayoutGroup>().cellSize.y - 5; 
			box.size = new Vector2(ratioX, ratioY);

			Rigidbody2D bdy = newSpace.AddComponent<Rigidbody2D>(); 

			float grav = 0; 

			bdy.gravityScale = grav; 
			bdy.constraints = RigidbodyConstraints2D.FreezeAll;  // if we dont freeze all constraints then all the UI pieces will fly all over the place.
			 

			listOfButtons.Add(newSpace);  // add the button to a list of buttons.

			counter ++;
		}

	}

	/**
	 * void buildPic():
	 * 
	 * gathers a clone of the backPack dictionary, this is the current backpack when this function is called.
	 * will count through the backPack Currently, and change the image on the button to the image of the item found.
	 * 
	 * return: Nothing, Void.
	 * 
	 * 
	 */ 
	public void buildPic(){
		Dictionary<string, List<item>> backPack = backPackManager.GetComponent<backPackManager> ().returnDictionary ();
		item [] itemHolder = new item[backPackManager.GetComponent<backPackManager>().returnBPListcount()];

		foreach(KeyValuePair<string, List<item>> counter in backPack){

			for(int k = 0; k < counter.Value.Count; k++){

				if(counter.Value[k].isItemInMenu == false){

					for(int j = 0; j < listOfButtons.Count; j ++){


						if(listOfButtons[j].name.Equals("emptySpace") && counter.Value[k].isItemInMenu == false){
							counter.Value[k].isItemInMenu = true;

							GameObject PhysicalItem = counter.Value[k].returnPhysicalItem(); 

							// we want each UI sprite to be changed to the object that we want to store in the UI panel.
							listOfButtons [j].GetComponent<Button> ().GetComponent<Image> ().sprite = PhysicalItem.GetComponent<SpriteRenderer> ().sprite; 
							
							// once I make my own sprites I will remove this line:
							listOfButtons [j].GetComponent<Button> ().GetComponent<Image> ().color = PhysicalItem.GetComponent<SpriteRenderer> ().color; 
							
							listOfButtons [j].gameObject.name = counter.Key; 


						}


					}

				}

			}

		}
				
	}



	public void removePic(GameObject itemToRemove){


		int counter = 0; 

		while(counter < listOfButtons.Count){

			if(itemToRemove.Equals(listOfButtons[counter])){


				listOfButtons[counter].GetComponent<Button>().GetComponent<Image>().sprite = prefab.GetComponent<Button>().GetComponent<Image>().sprite; 
				listOfButtons[counter].GetComponent<Button>().GetComponent<Image>().color = prefab.GetComponent<Button>().GetComponent<Image>().color; 
				listOfButtons[counter].gameObject.name = "emptySpace";

			}

			counter ++; 

		}


	}




	public GameObject returnQPanel(){

		return qpPanel; 
	}




	// Use this for initialization
	void Start () {
		backPackManager = GameObject.FindGameObjectWithTag("BPManager");
		newSize = 0; 
		qpPanel.SetActive (false);
		buildScroll (); 

	}
	
	// Update is called once per frame
	void Update () {                                          



		if (backPackManager.GetComponent<backPackManager> ().returnBackPacksCount() != 0) {

			if(newSize == backPackManager.GetComponent<backPackManager> ().returnBPListcount() ){
				//print (newSize);
				return;

			}else if (newSize != backPackManager.GetComponent<backPackManager> ().returnBPListcount() || newSize >= backPackManager.GetComponent<backPackManager> ().returnBPListcount()){


				newSize = backPackManager.GetComponent<backPackManager> ().returnBPListcount(); 

				buildPic(); 
			}


		}

	}
}
