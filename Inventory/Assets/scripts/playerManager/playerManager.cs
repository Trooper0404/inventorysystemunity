﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// basic PlayerManager, movement, and accessing PlayerIventory.
public class playerManager : MonoBehaviour {

	public Rigidbody2D body; 
	public float speed; 
	public GameObject BP;

	// when the player collides with a item object, we will store it and  destory it from the game
	void OnCollisionEnter2D(Collision2D collision){

		bool wasItemStored = false; 

		// we want to hit E on the item(this is a later build item.)
		if ( collision.gameObject.tag == "item") {

			item newItem = new item(collision.gameObject.name, collision.gameObject); 

			 wasItemStored = BP.GetComponent<backPackManager>().addToBackPack(newItem.returnName(), newItem); 

		}

		if (wasItemStored == true) {
			
			
			collision.gameObject.SetActive (false); 
			
		}


	}

	// Use this for initialization
	void Start () {
		BP = GameObject.FindGameObjectWithTag ("BPManager");
	}
	
	// Update is called once per frame
	void Update () {

		// Basic movement:
		if (Input.GetKey(KeyCode.A)) { // left
			
			
			body.AddForce(Vector2.left * speed);
			
		}
		
		if (Input.GetKey(KeyCode.W)) { // up
			
			
			body.AddForce(Vector2.up * speed);
			
		}
		if (Input.GetKey(KeyCode.D)) { // right
			
			
			body.AddForce(Vector2.right * speed);
			
		}
		if (Input.GetKey(KeyCode.S)) { // down
			
			
			body.AddForce(Vector2.down * speed);
			
		}

	}
}
